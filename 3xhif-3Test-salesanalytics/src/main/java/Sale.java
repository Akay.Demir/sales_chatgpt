import java.time.LocalDate;

public class Sale {

    private LocalDate saleDate;
    private double price;
    private String employeeName;
    private String brand;
    private String category;


    public static Sale ofCSVString(String csv) throws IllegalArgumentException {
       // TODO implement parsing of csv and return a sale
        return null; // FIXME
    }


    public Sale(LocalDate saleDate, double price, String employeeName,String category, String brand) {
        // TODO implement checks and set fields
    }

    public LocalDate getSaleDate() {
        return saleDate;
    }

    public double getPrice() {
        return price;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getBrand() {
        return brand;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "saleDate=" + saleDate +
                ", price=" + price +
                ", employeeName='" + employeeName + '\'' +
                ", brand='" + brand + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}

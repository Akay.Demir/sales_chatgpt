import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;

public class SalesAnalytics {

    private List<Sale> saleList;

    public void readFromCSV(String filename) throws FileNotFoundException {
       // TODO read from CSV
    }

    public Map<String, List<Sale>> getSalesOfEmployee(){
        // TODO get Sales per Employee
        return Collections.emptyMap();
    }

    public SortedMap<LocalDate, List<Sale>> getSalesPerDay(LocalDate from, LocalDate to){
        //TODO implement filtering
        return new TreeMap<>();
    }

    private List<Sale> filter(Predicate<Sale> filter) {
        // TODO implement filtering
        return Collections.emptyList();
    }

    public List<Sale> filterSellerAndBrand(String employee, String brand) {
        // TODO use filter(Predicate<Sale> filter) with a fitting lambda
        return Collections.emptyList();
    }

    public static <T> void printSales(Map<T, List<Sale>> sales) {
        //TODO BONUS!  implement
    }

    public static void main(String[] args) throws FileNotFoundException {
        // TODO you can try your methods here.
    }














    // Can't touch this
    /////////////////////////////////////////////////////////////////////////////
    public List<Sale> getSaleList() {
        return saleList;
    }

    public void setSaleList(List<Sale> sales){
        this.saleList = new ArrayList<>(sales);
    }

}
